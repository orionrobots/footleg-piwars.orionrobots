---
layout: post
title: Lift mechanism progress
date: 2020-12-06
categories: [tidy-the-toys]
tags: [piwars, 3dprinter, freecad]
---
Today I've made some further progress on the lift mechanism for Tidy the toys.
It's been slow going but I've been modelling a frame in Freecad.
The frame is unpowered, but should demonstrate things moving - I'm not sure if it will need any bearings, so lets see what I can get away with.

There is a main fixed frame, suspending two 4mm rods as linear bearings. On those are a carriage that can be raised/lowered.
The carriage has a further two rods, this time for the paddles/grippers to move.

Finally the paddles have some featuring to help increase gri. I have no idea if that will work.

I am yet to work back in the powered/servo motor aspect of the paddles, and the mechanism to raise/lower the carriage.

I am also going to experiment with threaded inserts - heat insert types. I've not used them before, except in a test part - which was so-so in terms of them being well-aligned.

The items have been assembled using the Freecad assembly4 workbench - a bit of a first for me. I am expecting to 3d print all the parts, other than the 4mm rods, for which I will cut some steel rods I have.

{% include image-gallery.html folder="/galleries/06-12-2020-lift-mechanism" %}

I will test this assembly out first, then enhance with the servo motor mounts, and racks/pinions and gears to move the paddles.

## Threaded insert experiment

For my threaded insert experiment, I used some cheap brass thermal inserts from Amazon. I 3d printed a test part, with 3 different hole sizes, based on the inserts outer dimension of 5mm - 5.2mm, 5mm, 4.8mm. I also added a 0.5mm chamfer to each hole - to try help with alignment. Not sure that helped like I thought it would.

I used my normal soldering iron at 312 degrees with its normal bit. The all sank a little too far. The one at exactly 5mm seemed to make the most sense.

I will try again, only instead of the chamfer, I'll place a 5.4mm, 2mm deep counterbore above the holes which should force it's alignment more (maybe 5.3mm - so it loosely fits, but not to loosely), and then sink that with the iron.

I may try a lower temperature too - which might give me more control over the process.

## Todo list

* Trying further threaded insert experiment as I want to use it in the lift frame.
* printing and assembling the lift frame.
* Testing it - refining for flaws.
* Motorising the opening/closing of the carriage - with print and test.
* Motorising the life/lowering of the carriage.
