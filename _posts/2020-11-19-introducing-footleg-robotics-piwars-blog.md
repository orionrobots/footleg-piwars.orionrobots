---
layout: post
title:  "Footleg Robotics Piwars 2021 Blog"
date:   2020-11-19
categories: [robotics, piwars, announce]
---
This is where we will share updates, and announcements on Orions Foot - the working name for a [Piwars 2021](https://piwars.org/2021-vpw/) robot.

We'll start by introducing co-conspirators:
* Dr Footleg
* Danny Staple (aka Orion)
* Helena (Mini_Orionrobots) on design/art/style

Our robot so far is a 4 wheel drive small bot, driven of course by a Raspberry Pi. 
Currently Orion is working on a lift-and-grab mechanism for the Tidy up the Toys challenge.
Dr Footleg is working on the main PCB for interfacing motors, sensors and other IO neatly with the Raspberry Pi along with power distribution.
