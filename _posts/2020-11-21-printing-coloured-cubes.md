---
layout: post
title:  "Tidy The Toys Cubes Printing"
date:   2020-11-19
categories: [orion, 3dprinter]
tags: [piwars, 3dprinter, flashforge-finder]
---
[Tidy the toys](https://piwars.org/2021-vpw/challenges/tidy-up-the-toys/) is a challenge that involves a set of coloured cubes to pick up.

These should be in the primary colours - red, green and blue.

I've been able to print these cubes on my Flashforge finder printer, but since I needed different colours, I needed to fix a few things first.

## Colour Filament Changing

I needed to be able to change the filament colour. The printer I have does not have dual head or anything fancy, and the filament spool is usually stored in a cassette that goes into the back. You can fit any spool off the right size in, but I don't really want to have to do that if I'm switching colour. I also have a set of [Faberdashery](http://www.faberdashery.co.uk) spools with colour swatches to play with to make different colours - it's only a few meters of each, so they need too go onto temporary spools.

To handle changing these out, I've added an external spool holder to my printer. I grabbed a design for this from [thingiverse](https://www.thingiverse.com/make:847126). This design clips almost perfectly into the recess for the removal catch on the filament cartridge.

{% include image-gallery.html folder="/galleries/printing-coloured-cubes/holder" %}

This holder has worked brilliantly and allowed me to print with different spools and colours.

## Cubes I've printed so far

So far I've printed a green cube, although it's a little lime green, and the blue cube. I'm yet to print red. Changing the filament still means getting it all the way out of the extruder and inserting another filament until the colour runs cleanly.

I also had a failed attempt with the blue - with some major warping - although that came after I'd swapped my print bed (another story).

{% include image-gallery.html folder="/galleries/printing-coloured-cubes/cubes" %}

All is left is the red cube. I've been doing this while working on the other aspects.
