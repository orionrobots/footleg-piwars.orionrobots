---
layout: post
title:  "Orion story so far"
date:   2020-11-19
categories: [announce]
tags: [robotics, piwars, announce, 3dprinter, flashforge-finder]
---
Hi, I'm Orion, Danny Staple.

I've been building and designing parts for this robot for a few weeks already, with mixed successes.
In the coming days, I'll fill out what I've already been doing, and link out to them from this post.

First, lets start with the easy stuff, the props.

## Tidy the toys props

[Tidy the toys](https://piwars.org/2021-vpw/challenges/tidy-up-the-toys/) is a challenge that involves a set of coloured cubes to pick up.

I've been able to print these cubes on my Flashforge finder printer, but since I needed different colours, I needed to be able to change the filament colour.

This meant also having an external spool - so I grabbed a design for this from Thingiverse. [Printing Coloured Cubes]({% post_url 2020-11-21-printing-coloured-cubes %})

## The control board

Dr Footleg has shared a 3D model of the control board, also part of the chassis that he is designing. I've taken this, cleaned it up a bit for 3D printing, and printed it for use in scaling and testing things for the robot.

[![3D Printed Control board](/thumbnails/orions-story-so-far/3d-printed-control-board-mockup.jpg)](/galleries/orions-story-so-far/3d-printed-control-board-mockup.jpg))
