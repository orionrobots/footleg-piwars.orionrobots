FROM jekyll/jekyll:3.4.0
RUN apk update && apk add imagemagick
WORKDIR /srv/jekyll
RUN mkdir -p /srv/jekyll
COPY Gemfile /srv/jekyll
COPY Gemfile.lock /srv/jekyll
RUN cd /srv/jekyll; bundle install
COPY build.sh /srv/jekyll
ENTRYPOINT [ "/bin/bash", "/srv/jekyll/build.sh" ]
# ENTRYPOINT [ "/srv/jekyll/build.sh" ]