---
layout: page
title: About
permalink: /about/
menu: main
---
This is the blog where we will share updates, and announcements on Orions Foot - the working name for a [Piwars 2021](https://piwars.org/2021-vpw/) robot. A Raspberry Pi based small wheeled robot.

The Orions Foot team consists of:
* Dr Footleg
* Danny Staple (aka Orion)
* Helena (Mini_Orionrobots) on design/art/style
