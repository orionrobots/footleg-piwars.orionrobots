#1/bin/bash
mkdir -p thumbnail

for file in $(cd galleries; find . -type d -print); do
    mkdir -p "thumbnails/${file}"
done

(cd galleries; find . -type f -print0) | while read -d $'\0' file
do
    convert "galleries/${file}" -resize 300x300^ -gravity Center -crop 300x300+0+0 "thumbnails/${file}"
done
bundle exec jekyll $@
