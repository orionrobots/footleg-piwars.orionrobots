---

## Setup

With docker installed, build the image with:

    docker build . -t footleg-2021-blog

You should only need to repeat this if the Dockerfile or Gemfile change.

Running in docker:

    docker run --rm -v $(pwd):/srv/jekyll -p 8080:4000 -it footleg-2021-blog serve -H 0.0.0.0

---

## Getting Started

Remember you need to wait for your site to build before you will be able to see your changes.  You can track the build on the **Pipelines** tab.

### Read more

Read more at Jekyll's [documentation][].

## Troubleshooting

1. CSS is missing! That means two things:
    * Either that you have wrongly set up the CSS URL in your templates, or
    * your static generator has a configuration option that needs to be explicitly
    set in order to serve static assets under a relative URL.

[ci]: https://about.gitlab.com/gitlab-ci/
[Jekyll]: http://jekyllrb.com/
[install]: https://jekyllrb.com/docs/installation/
[documentation]: https://jekyllrb.com/docs/home/
[userpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#user-or-group-pages
[projpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#project-pages
